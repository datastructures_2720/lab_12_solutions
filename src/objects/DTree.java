package objects;

import interfaces.IDTree;
import objects.DLine;

/**
 *This class should implement the interface <code>IDTree<DLine></code>.
 * <br>
 * This class creates a binary (search) tree with nodes of type <code>DLine</code>. A DTree
 * follows these properties:
 * 		- It has a root DLine that is parent/ancestor of all nodes.
 * 		- For each DLine dl, the following conditions should always be held:
 * 			- dl.length > dl.leftChild.length
 * 			- dl.length <= dl.rightChild.length
 * 
 * @author Azim Ahmadzadeh
 *
 */
public class DTree implements IDTree<DLine> {

	private DLine root;
	private int n;

	public DTree() {
		this.root = null;
		this.n = 0;
	}

	public void insert(DLine dl) {
		this.root = this.insertRec(this.root, dl);
		this.n++;
	}

	private DLine insertRec(DLine currentDLine, DLine dl) {

		// If this node does not exist
		if (currentDLine == null) {
			return new DLine(dl);
		}

		// If the new dline is shorter than the current dline,
		// add dl to the left of currentDLine
		if (dl.length < currentDLine.length) {
			currentDLine.setLeftChild(insertRec(currentDLine.getLeftChild(), dl));
		} else if (dl.length >= currentDLine.length) {
			currentDLine.setRightChild(insertRec(currentDLine.getRightChild(), dl));
		}

		return currentDLine;
	}

	public DLine getRoot() {
		return this.root;
	}

	@Override
	public int getTreeLevel(DLine dl) {

		if (dl == null)
			return 0;

		int leftLevel = this.getTreeLevel(dl.getLeftChild());
		int rightLevel = this.getTreeLevel(dl.getRightChild());
		return Math.max(leftLevel, rightLevel) + 1;
	}

	@Override
	public int getNumberOfNodes() {

		return 0;
	}
}
