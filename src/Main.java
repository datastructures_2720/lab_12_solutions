import blackbox.BTreePrinter;
import interfaces.IDTree;
import objects.DLine;
import objects.DTree;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -> Class: Data Structures - 2720 - - - - - - - - - - - - - - - - - - - - - -
 * -> LAB: 12 [Solutions] - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Date: Friday 09 Nov, 2018 - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Subject: Binary Tree- - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Lab Web-page: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * [https://www.baeldung.com/java-binary-tree]
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/] - - - - -
 */
public class Main {

	public static void main(String[] args) {
		
		IDTree<DLine> myTree = new DTree();
		myTree.insert(new DLine(10));
		myTree.insert(new DLine(14));
		myTree.insert(new DLine(4));
		myTree.insert(new DLine(3));
		myTree.insert(new DLine(17));
		myTree.insert(new DLine(27));
		myTree.insert(new DLine(9));
		myTree.insert(new DLine(11));
		BTreePrinter.printNode(myTree.getRoot());
		
		System.out.println(myTree.getTreeLevel(myTree.getRoot()));
	}
}
